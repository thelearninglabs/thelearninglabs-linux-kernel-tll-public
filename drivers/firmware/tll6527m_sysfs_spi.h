/**
 * @file tll6527m_sysfs_spi.h
 *
 * @brief
 * The file creates a sysfs node in /sys/firmware/tll6527m/spi/ph9device
 *
 * @version 1.1 
 * 
 * @date 20121010
 * 
 * @author Karthikeyan GA
 *
 * XqJv Copyright(c) 2010-2012 The Learning Labs,Inc. All Rights Reserved  jVkB
 *
 * Organization: The Learning Labs
 *
 * Tested: Compiler bfin-linux-uclinux-gcc; Output format FDPIC module;
 * Target TLL6527M V1.2
 *
 * Revised : 20121010, Author: Karthikeyan GA, Notes: Added the spi board info
 * in the tll_hotswap_spi_devices_board_info array for the custom driver
 * for timer_spi_adc
 * 
 * Created: 20120825, Author: Karthikeyan GA, Notes: Created initial version
 *
 * References:
 *
 * 1. Sample code for creating simple node in sysfs 
 *    http://geekwentfreak.wordpress.com/2010/06/28/
 *    create-files-in-sysfs-sysfs_create_group/
 * 2. The spi summary page from kernel.org 
 *    http://www.kernel.org/doc/Documentation/spi/spi-summary
 * 3. The ADI engineering zone giving a sample code 
 *    http://ez.analog.com/message/58120#58120
 *
 */
#include<linux/module.h> //kobject and module related structures
#include<linux/init.h>   //module_init and module_exit macros
#include <asm/portmux.h> // GPIO_PH9 is res
#include <linux/spi/spi.h> //spi related functions, structures and macros 
#include <asm/bfin5xx_spi.h> //blackfin spi related macros and structures
#include <../drivers/staging/iio/iio.h> //iio framework
#include <../drivers/staging/iio/adc/ad7476.h> //ad7476 related structures

/**
 * Number of sub folders that will be created as part of one bus numbers
 *  0 - <spi_bus_number> folder
 *  1 - generic folder (sub folder of <spi_bus_number>)
 *  2 - controller_data folder (sub folder of generic)
 */
#define TLL6527M_SYSFS_SPI_NUM_FOLDER 3

/**
 * Number of file nodes in <spi_bus_number> folder
 * <device_node_name>
 */
#define TLL6527M_SYSFS_SPI_BUS_NUM_NODES_COUNT 1 

/**
 * Number of file nodes in generic folder
 *  chip_select, modalias, max_speed_hz, mode
 */
#define TLL6527M_SYSFS_SPI_GENERIC_NODES_COUNT 4

/**
 * Number of file nodes in controller_data folder
 *  ctl_reg, enable_dma, cs_chg_udelay, idle_tx_val, pio_interrupt
 */
#define TLL6527M_SYSFS_SPI_CONTROLLER_DATA_NODES_COUNT 5

#define GENERIC_NODE_NAME "generic"
#define CHIP_SELECT_NODE_NAME chip_select
#define MODALIAS_NODE_NAME modalias
#define MAX_SPEED_HZ_NODE_NAME max_speed_hz
#define SPI_MODE_NODE_NAME spi_mode
#define CONTROLLER_DATA_NODE_NAME "controller_data"
#define CTL_REG_NODE_NAME ctl_reg
#define ENABLE_DMA_NODE_NAME enable_dma
#define CS_CHG_UDELAY_NODE_NAME cs_chg_udelay
#define IDLE_TX_VAL_NODE_NAME idle_tx_val
#define PIO_INTERRUPT_NODE_NAME pio_interrupt

#define TLL6527M_NODE "tll6527m" //to be wrapped in " as it is used as string
#define TLL6527M_SPI_NODE "spi"  //to be wrapped in " as it is used as string
#define PH9_NODE "cs_ph9device"

#define TLL6527M_MAX_SPI_BUS 2

#define __ATTR_REDEF(_var,_name,_mode,_show,_store){ \
	_var.attr.name=__stringify(_name); \
	_var.attr.mode=_mode; \
	_var.show=_show; \
	_var.store=_store;\
}

#define __ATTR_REDEF_STR(_var,_name,_mode,_show,_store){ \
	_var.attr.name=_name; \
	_var.attr.mode=_mode; \
	_var.show=_show; \
	_var.store=_store;\
}


struct tll6527m_sysfs_spi_tree {
    u16 spi_bus_num;
    char device_node_name[SPI_NAME_SIZE];
    char current_modalias_name[SPI_NAME_SIZE];
    struct spi_board_info generic_device_info;
    struct bfin5xx_spi_chip controller_data;
    struct spi_board_info *hot_swap_device_list;
    int hot_swap_devices_count;
    int *allowed_cs_values;
    int allowed_cs_values_count;
    //Below members are not to be initialised. They will be initialised by
    //init function of the tree
    struct spi_device *current_device;
    struct kobject *folders_kobj[TLL6527M_SYSFS_SPI_NUM_FOLDER];
    struct kobj_attribute bus_num_nodes[TLL6527M_SYSFS_SPI_BUS_NUM_NODES_COUNT];
    struct kobj_attribute generic_nodes[TLL6527M_SYSFS_SPI_GENERIC_NODES_COUNT];
    struct kobj_attribute \
          controller_data_nodes[TLL6527M_SYSFS_SPI_CONTROLLER_DATA_NODES_COUNT];
    struct attribute_group file_node_attr_group[TLL6527M_SYSFS_SPI_NUM_FOLDER];
    
    struct attribute *bus_num_node_attr \
                [TLL6527M_SYSFS_SPI_BUS_NUM_NODES_COUNT+1];
    struct attribute *generic_node_attr \
                [TLL6527M_SYSFS_SPI_GENERIC_NODES_COUNT+1];
    struct attribute *controller_data_node_attr \
                [TLL6527M_SYSFS_SPI_CONTROLLER_DATA_NODES_COUNT+1];
};

/** 
 * @brief
 * tll6527m_sysfs_spi_tree_init
 *
 *  This function initialises the different kobject related members
 * in the struct tll6527m_sysfs_spi_tree for creating the nodes. Then it creates
 * the nodes and returns 0
 *
 * @param tree_info the pointer of tll6527m_sysfs_spi_tree which has to be used
 * for creating the tree folder structure
 * @param parent_kobj the pointer of kobject of the parent node under which the 
 * tree has to be created
 * @return int 0 on success, others on failure
 */
int tll6527m_sysfs_spi_tree_init(struct tll6527m_sysfs_spi_tree *tree_info,
                                 struct kobject *parent_kobj);

/** 
 * @brief
 * tll6527m_sysfs_spi_tree_release
 *  This function removes the generic foder structure and the bus number
 * folder node denoted by the tree_info
 *
 * @param tree_info the pointer of tll6527m_sysfs_spi_tree which has to be used
 * for removing the tree folder structure
 * @return int 0 on success and others on failure
 */
int tll6527m_sysfs_spi_tree_release \
                      (struct tll6527m_sysfs_spi_tree *tree_info);
