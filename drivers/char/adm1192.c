/*
 * File: adm1192.c
 *
 * This driver provides interface to access ADM1192 power controller
 * chip on TLL6527M platform.
 *
 * Copyright (C) 2009 Hemanth Varadha, MindTree LTD.
 * Licensed under the GPL-2 or later.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/i2c.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/delay.h>
#include <linux/uaccess.h>

#define CURR_MON_CDEV_NAME "adm1192"

struct adm1192_st {
	int major;
	struct cdev classdev;
	struct class *class;
	struct i2c_client *i2c_client;
} adm1192;

struct i2c_device_id adm1192_id[] = {
	{"adm1192",  0},
	{}
};

static int adm1192_i2c_probe(struct i2c_client *client,
				const struct i2c_device_id *id);
static int adm1192_i2c_remove(struct i2c_client *client);

int adm_open(struct inode *in, struct file *lfile)
{
	/* Do nothing */
	return 0;
}

static ssize_t adm_read(struct file *file, char __user *buf,
			 size_t count, loff_t *ppos)
{
	unsigned char data[3];
	int i;

	if (!adm1192.i2c_client)
		return -ENODEV;

	/* Enable the current/voltage continuous conversion */
	data[0] = 0x15;
	i2c_master_send(adm1192.i2c_client, data, 1);

	/* 400 us delay is required for adm to store cur/vol
	 * values into registers */
	udelay(400);
	i2c_master_recv(adm1192.i2c_client, data, 3);

	for (i = 0; i < 3; i++) {
		if (put_user(data[i], (unsigned char __user *)buf+i))
			break;
	}

	return i;
}

struct file_operations adm_fops = {
	.open = adm_open,
	.read = adm_read,
};

static int adm1192_i2c_probe(struct i2c_client *client,
		const struct i2c_device_id *id)
{
	int rc;

	adm1192.i2c_client = NULL;

	rc = i2c_check_functionality(client->adapter, I2C_FUNC_I2C);
	if (!rc) {
		dev_err(&client->dev,
			"This bus doesn't support raw I2C operation\n");
		return -EINVAL;
	}

	adm1192.i2c_client = client;
	if (NULL == client)
		return -ENODEV;

	/* Provide the /dev/adm1192 interface to the user to
	 * access the ADM1192 */
	adm1192.major = register_chrdev(0, CURR_MON_CDEV_NAME, &adm_fops);
	if (adm1192.major < 0) {
		dev_err(&client->dev, "Failed to register char dev\n");
		goto probe_exit;
	} else {
		adm1192.class = class_create(THIS_MODULE, "adm1192");
		if (IS_ERR(adm1192.class)) {
			dev_err(&client->dev, "Failed to create adm class\n");
			goto unreg_dev;
		} else {
			cdev_init(&adm1192.classdev, &adm_fops);
			adm1192.classdev.owner = THIS_MODULE;
			rc = cdev_add(&adm1192.classdev,
					MKDEV(adm1192.major, 0), 1);
			if (rc < 0) {
				dev_err(&client->dev, "fail to add cdev\n");
				goto rel_class;
			} else {
				device_create(adm1192.class, NULL,
					MKDEV(adm1192.major, 0),
					NULL, "adm1192");
			}
		}
	}
	return 0;

rel_cdev:
	cdev_del(&adm1192.classdev);
rel_class:
	class_destroy(adm1192.class);
unreg_dev:
	unregister_chrdev(adm1192.major, CURR_MON_CDEV_NAME);
probe_exit:
	return -ENODEV;
}

static int adm1192_i2c_remove(struct i2c_client *client)
{
	adm1192.i2c_client = NULL;
	device_destroy(adm1192.class, MKDEV(adm1192.major, 0));
	cdev_del(&adm1192.classdev);
	class_destroy(adm1192.class);
	unregister_chrdev(adm1192.major, CURR_MON_CDEV_NAME);

	return 0;
}

struct i2c_driver adm1192_i2c_driver = {
	/*.id = 0, */
	/* CL, The 2010R1 distribution does not have 'id' as member of struct
 	 * i2c_driver
	 */
	.driver = {
		.name = "current monitor",
	},
	.probe = adm1192_i2c_probe,
	.remove = adm1192_i2c_remove,
	.id_table = adm1192_id
};

int __init adm1192_init(void)
{
	return i2c_add_driver(&adm1192_i2c_driver);
}
module_init(adm1192_init);

void __exit adm1192_exit(void)
{
	return i2c_del_driver(&adm1192_i2c_driver);
}
module_exit(adm1192_exit);

MODULE_AUTHOR("Hemanth Varadha");
MODULE_DESCRIPTION("ADM1192 - current monitor I2C Bus Driver");
MODULE_LICENSE("GPL");
